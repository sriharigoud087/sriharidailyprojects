import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-fourth-component',
  templateUrl: './fourth-component.component.html',
  styleUrls: ['./fourth-component.component.css'],
})
export class FourthComponentComponent implements OnInit {
  constructor(private active: ActivatedRoute) {}

  ngOnInit(): void {
    //  this.active.snapshot.params.id(1);
  }
}
