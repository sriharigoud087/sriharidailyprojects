import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
@Component({
  selector: 'app-first-component',
  templateUrl: './first-component.component.html',
  styleUrls: ['./first-component.component.css'],
})
export class FirstComponentComponent implements OnInit {
  car: any;
  currentURL: string = '';
  constructor(private router: Router, private route: ActivatedRoute) {}
  ngOnInit(): void {
    this.car = this.route.snapshot.paramMap.get('carBrand'); // Snapshot param
    this.currentURL = this.router.url;

    this.router.events.subscribe((val) => {
      this.currentURL = this.router.url;
    });
  }
}
