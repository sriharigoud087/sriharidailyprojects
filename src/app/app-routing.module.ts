import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FirstComponentComponent } from './first-component/first-component.component';
import { FourthComponentComponent } from './fourth-component/fourth-component.component';
import { LoginComponent } from './login/login.component';
import { SecondComponentComponent } from './second-component/second-component.component';
import { ThirdComponentComponent } from './third-component/third-component.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {
    path: 'cars/:carBrand',
    component: FirstComponentComponent,
    canActivate: [],
  },
  { path: 'second', component: SecondComponentComponent },
  { path: 'third', component: ThirdComponentComponent },
  { path: 'fourth/id', component: FourthComponentComponent },
  { path: 'login', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
